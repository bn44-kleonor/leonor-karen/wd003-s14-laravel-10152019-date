<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- PAYPAL -->
    <script src="https://www.paypal.com/sdk/js?client-id=AYcehnmgTA3gZWnmNABVzSBuC2_uZGbjKdnbvNdmNI6XVXd5ocsozaS4oaMwHZyK3uU75w8GWkCgy8dc"></script>

    <!-- CHART JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <!-- <script src="{{ asset('js/app.js') }}" defer></script> -->

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- TIME -->
<!--     <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.14/moment-timezone.min.js"></script> -->

    @yield("timezone")

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        @include("partials.navbar")
        @include("partials.alerts")

        <main class="py-4 mb-5">
            <div class="container" id="main-container">
                @yield('content')
            </div>
        </main>

        @include("partials.footer")
    </div>

    <!-- <<script src="{{ asset('js/app.js') }}"></script> -->
    <script type="text/javascript" src="{{asset('js/app.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/script.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/paypal.js')}}"></script>
    @yield("script");
</body>
</html>
