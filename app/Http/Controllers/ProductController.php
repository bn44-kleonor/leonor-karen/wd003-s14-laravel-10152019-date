<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use Carbon\Carbon;
use Auth;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Products";

        //1) this displays all products, "excluding" soft-deleted ones
        $products = Product::orderBy("name")->paginate(3);
        return view('products.index')->with('products', $products);
    
        //2) to display all products, "including" soft-deleted ones
        // $products = Product::withTrashed()->get();
    
        //3) to display only soft deleted products
        // $products = Product::onlyTrashed()->get();
        // $products = Product::onlyTrashed()->paginate(10);

        // dd($products);
        return view("products.index", compact('title', 'products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = "Edit Product Page";
        $product = Product::find($id);
        // dd($product->updated_at);
        // dd(Carbon::now);
        // dd(Carbon::now(Asia/Manila));

        //now in UTC
        dump(now());

        //now in user's timezone
        dump($product->tz(now()));

        //other timezones
        dump(Carbon::now("America/New_York"));

        //maninulation
        dump(Carbon::yesterday());
        dump(Carbon::tomorrow());
        dump(Carbon::today());
        dump(Carbon::now());
        dump(Carbon::now()->addSeconds(5));
        dump(Carbon::now()->addMintues(5));
        dump(Carbon::now()->addHours(5));
        dump(Carbon::now()->addDays(5));
        dump(Carbon::now()->addMonth());
        dump(Carbon::now()->addMonths(5));
        dump(Carbon::now()->addYears(5));

        //formatting
        dump(Carbon::now()->toDateString());    //2019-10-19
        dump(Carbon::now()->toFormattedDateString());   //December 9, 2019

        //time difference

        $categories = Category::all();
        return view("products.edit", compact("title", "product", "categories"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd("test");
        $this->validate($request, [
            'name' => 'required',
            "category" => "required|numeric",
            "price" => "required|numeric",
            "stock" =>  "required|numeric",
            "description" => "required"
            ]);
        // dd("test");
        $product = Product::find($id);
        $product->name = $request->input("name");
        $product->price = $request->input("price");
        $product->description = $request->input("description");
        $product->category_id = $request->input("category");
        $product->stock = $request->input("stock");

        //image
        // dd($request->file("image"));
        $image = $request->file("image");
        if($image != null) {
            //delete image sa folder
            //unlink($product->image);

            //set image name 
            $image_name = time() . "." . $image->getClientOriginalExtension();

            //identify where to save image
            $destination = "images/";

            //move image to new location
            $image->move($destination, $image_name);

            //save image to $product->image
            $product->image = "/" . $destination . "/" . $image_name;
        }

        $product->save();
        return redirect()->back()->with("success", "Changes has been saved!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product_name = $product->name;
        $product->delete();
        // return redirect()->back()->with("success", "$product_name has been deleted!");
        return redirect("/products")
        ->with("success", "$product_name has been deleted!")
        ->with("undo_url","/restore/$id");
    }

    public function restore($id)
    {
        //get softdeleted products
        $product = Product::onlyTrashed()->where("id", $id)->first();  //get returns a collection

        $product->restore();
        return back()->with("success", "$product->name has been restored!");
    }
}