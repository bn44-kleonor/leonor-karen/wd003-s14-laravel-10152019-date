<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Product;
use App\User;
use App\Order;
use Auth;
use Session;

class PageController extends Controller
{
    //index method - returns welcome.blade.php with all products
    public function welcome () 
    {
    	$products = Product::orderBy("stock", "desc")->paginate(3);
    	return view('welcome')->with('products', $products);
    }

    public function cart () 
    {
        //create an array where we will push our product & quantity
        $cart_products = [];

        //set total 
        $total = 0;

        //check if cart session exists to iterate
        if(Session::has("cart"))
        {

            $cart = Session::get("cart");
            foreach($cart as $id => $quantity)  //"quantity" nasa 'Session'
            {
                //find the product via its id
                $product = Product::find($id);

                //add quantity attribute to product
                $product->quantity = $quantity;

                //add subtotal attribute to product
                $product->subtotal = $product->price * $quantity;

                //get total
                $total += $product->subtotal;

                //array push product to cart_products variable
                $cart_products[] = $product;
            }
        }

        $title = "My Cart";
    	return view("pages.cart", compact('cart_products', "total", "title"));
    }

    public function checkout ()
    {
        //dd(Session::has("cart"));
        if(Session::has("cart") && Session::get("cart") != [])
        {
            $user = Auth::user();
            $title = "Checkout";
            $cart_products = [];
            $total = 0;
            $cart = Session::get("cart");
            foreach($cart as $id => $quantity)
            {
                $product = Product::find($id);
                $product->quantity = $quantity;
                $product->subtotal = $product->price * $quantity;
                $total += $product->subtotal;
                $cart_products[] = $product;
            }
            return view("pages.checkout", compact("title", "user", "cart_products", "total"));
        } else 
        {
            //return view("welcome")->with("error", "Please add products to cart before checkout");
            return redirect()->action("PageController@welcome")->with("error", "Please add products to cart before checkout");
        }
    }

    public function confirmation (Request $request, $id)
    {

        dd("test");

        //1) check if user details are filled out
        //   -fill out users table
        try{
            $this->validate($request, [
                'first_name' => 'required',
                'last_name' => 'required',
                'address' => 'required',
                'contact_number' => 'required',
            ]);
            // dd("test");
            $user = User::find($id);
            $user->first_name = $request->input("first_name");
            $user->last_name = $request->input("last_name");
            $user->address = $request->input("address");
            $user->contact_number = $request->input("contact_number");
            //dd("test");
            $user->save();

        } catch(\Exception $e){
            return redirect()->back()->with("error","Please fillout all fields.");
        }

        //2) inser data into orders table
        $order = new Order;
        $order->total = $request->input("total");
        $order->user_id = $id;
        $order->transaction_code = Str::random(5) . " - " . time();

        if($request->paypal_id){
            $order->payment_mode_id = 2;    //PAYPAL
            $order->status_id = 2;          //COMPLETED
            $order->paypal_id = $request->paypal_id;
        } else {
            $order->payment_mode_id = 1;    //1 for COD
            $order->status_id = 1;          //1 for 'pending'
        }

        $order->save();
        // dd($order);
        $order = Order::where('user_id', $id)->orderBy("created_at", "desc")->first();
        // dd($order);

        //3) insert data into product_order table
        $order_id = $order->id;
        $cart = Session::get("cart");
        foreach($cart as $product_id => $quantity){
            $product = Product::find($product_id);
            $product->orders()->attach($order_id, ['quantity' => $quantity]);
        }

        //4) destroy cart session
        Session::forget("cart");

        //5) display confirmation page with title, order, transaction_code and total
        $total = $order->total;
        $transaction_code = $order->transaction_code;
        $title = "Confirmation";

        //refactor if payviacod uses fetch
        if($request->paypal_id){
            return view("partials.confirmation", compact("title", "total", "transaction_code", "order"))->render();
        } else {
            return view("pages.confirmation", compact("title", "total", "transaction_code", "order"));
        }
    }

    public function search(Request $request){
        // dd($request->input("search"));
        // dd($request->search);
        $searchKey = $request->search;
        $products = Product::where("name", "like", "%" . $searchKey . "%")->paginate(10);
        // return view("partials.search")->with('products', $products)->render();
        if($products->count() > 0){
            return view("partials.search")->with('products', $products);
        } else {
            return "none";
        }
    }
}
