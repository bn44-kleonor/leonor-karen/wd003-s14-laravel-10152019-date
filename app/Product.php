<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Auth;

class Product extends Model
{
    use SoftDeletes;

    //many Products "belongs to many" Orders (MANY-to-MANY) ----revised below
    
	//a PRODUCT belongsToMany orders
    public function orders()
    {
        return $this->belongsToMany('App\Order')->withPivot("quantity")->withTimeStamps();
    }

    //a PRODUCT belongsTo a single category
    public function category()
    {
    	return $this->belongsTo('App\Category');
    }

    public function tz($value = null)
    {
        $user = Auth::user();
        //if no user is logged in, use default timezome;
        $timezone = $user ? $user->timezone : "UTC";

        // return default/user's timezone
        return Carbon::createFromTimestamp(strtotome($value))->timezone($timezone);
    }
}
